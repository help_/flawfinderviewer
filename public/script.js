document.addEventListener('DOMContentLoaded', () => {
    const uploadJsonBtn = document.getElementById('uploadJsonBtn');
    const jsonListContainer = document.getElementById('jsonList');
  
    // Function to fetch JSON entries from the API and display them
    async function fetchAndDisplayJsonEntries() {
      try {
        const response = await fetch('/api/json');
        const jsonEntries = await response.json();
  
        jsonListContainer.innerHTML = ''; // Clear previous entries
  
        jsonEntries.forEach(entry => {
          const entryElement = document.createElement('div');
          entryElement.textContent = `ID: ${entry._id}, Data: ${JSON.stringify(entry.data)}`;
          jsonListContainer.appendChild(entryElement);
        });
      } catch (error) {
        console.error('Error fetching JSON entries:', error);
      }
    }
  
    // Event listener for the "Upload JSON" button
    uploadJsonBtn.addEventListener('click', async () => {
      try {
        const jsonData = { key: 'value' }; // Replace with your JSON data
        const response = await fetch('/api/json', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(jsonData),
        });
  
        if (response.ok) {
          console.log('JSON data uploaded successfully.');
          fetchAndDisplayJsonEntries(); // Fetch and display updated JSON entries
        } else {
          console.error('Error uploading JSON data.');
        }
      } catch (error) {
        console.error('Error uploading JSON data:', error);
      }
    });
  
    // Fetch and display initial JSON entries when the page loads
    fetchAndDisplayJsonEntries();
  });
  