# Use an official Node.js runtime image as the base image
FROM node:14

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the container
# COPY server/package*.json ./

# install new npm
RUN npm init -y

# Install Express globally
RUN npm install express mongoose body-parser


# Copy the server code
# COPY server ./server

# # Copy the website code
# COPY public ./public
COPY . .

# Expose the server port
EXPOSE 3000

# Command to run the server
CMD [ "node", "server/server.js" ]
#ENTRYPOINT ["tail", "-f", "/dev/null"]
