const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());

// Connect to MongoDB
mongoose.connect('mongodb://mongodb:27017/mydb', {
useNewUrlParser: true,
useUnifiedTopology: true,
});

// Define a schema and model for your JSON data
const jsonDataSchema = new mongoose.Schema({
data: Object,
});
const JsonData = mongoose.model('JsonData', jsonDataSchema);


// Handle root URL - serve the 'index.html' file
app.get('/', (req, res) => {
res.sendFile(path.join(__dirname, '../public', 'index.html'));
});

// Create a new JSON data entry
app.post('/api/json', async (req, res) => {
try {
    const jsonData = new JsonData({ data: req.body });
    await jsonData.save();
    res.status(201).json({ message: 'JSON data created.' });
} catch (error) {
    res.status(500).json({ error: 'Internal server error.' });
}
});

// Read all JSON data entries
app.get('/api/json', async (req, res) => {
try {
    const jsonEntries = await JsonData.find();
    res.json(jsonEntries);
} catch (error) {
    res.status(500).json({ error: 'Internal server error.' });
}
});

// Update a JSON data entry
app.put('/api/json/:id', async (req, res) => {
try {
    const id = req.params.id;
    await JsonData.findByIdAndUpdate(id, { data: req.body });
    res.json({ message: 'JSON data updated.' });
} catch (error) {
    res.status(500).json({ error: 'Internal server error.' });
}
});

// Delete a JSON data entry
app.delete('/api/json/:id', async (req, res) => {
try {
    const id = req.params.id;
    await JsonData.findByIdAndDelete(id);
    res.json({ message: 'JSON data deleted.' });
} catch (error) {
    res.status(500).json({ error: 'Internal server error.' });
}
});

app.listen(PORT, () => {
console.log(`Server is running on port ${PORT}`);
});
  

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
